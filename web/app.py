from flask import Flask,render_template
import os
DOCROOT = "./pages"
# import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

app = Flask(__name__)

@app.route("/<path:sock>")

def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
        # first check if the name is end by html or css
        # if yes then sent the name to next check 
    if (".html" in sock) or (".css" in sock):
        # use in methond to find if the name contains the 
        # invalid symbol 
        # if contains then transmit the 403 forbidden error 
        # if not then send to the next check
        if ((sock.find("`") == -1) and (sock.find("..") == -1) and (sock.find("//") == -1)):            
            # if True transmit the ok
            # if False tranmit the 404 not found error
            return render_template(sock)
        else:
            return report_403_error("")
    else:
        return report_404_error("")

@app.errorhandler(403)
def report_403_error(error):
    return render_template("403.html")

@app.errorhandler(404)
def report_404_error(error):
    return render_template("404.html")

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')

   